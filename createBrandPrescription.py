import traceback
import logging
import json
import pymysql
import uuid
import sys
import os

import time

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def openConnection():
    """Generic MySQL DB connection handler"""
    global Connection
    try:
        Connection = pymysql.connect(os.environ['v_db_host'], os.environ['v_username'], os.environ['v_password'], os.environ['v_database'], connect_timeout=5)
    except Exception as e:
        logger.error(e)
        logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
        raise e


def fn_checkBrandExist(lv_CutitronicsBrandID=None):

    """
    Name - fn_CheckBrandExists

    Description - Checks the backend database to see if the CutitronicsBrandID is valid

    Arguments -
        lv_CutitronicsBrandID (String)

    Returns -
        500 - Failure, the user does not exist in the back office.
        Success - Cutitronics BrandID is returned, brand exists in the back office
    """
    lv_list = ""
    lv_statement = "SELECT CutitronicsBrandID, BrandName FROM BrandDetails WHERE (CutitronicsBrandID = %(CutitronicsBrandID)s)"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsBrandID': lv_CutitronicsBrandID})
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))

            if len(lv_list) == 0:
                return(500)
            else:
                return lv_list
    except pymysql.err.IntegrityError as e:
        logger.info(e.args)
        return(500)


def fn_CheckTherapistExists(lv_CutitronicsTherapistID, lv_CutitronicsBrandID):
    """
    Name - fn_CheckTherapistExists

    Description - Checks the backend database to see if the CutitronicsTherapistID is valid

    Arguments -
        lv_CutitronicsTherapistID (String)
        lv_CutitronicsBrandID (String)

    Returns -
        300 - Failure, the user does not exist in the back office.
        Success - Cutitronics UserID is returned, user exists in the back office
    """

    lv_list = ""
    lv_statement = "SELECT CutitronicsTherapistID, TherapistName FROM TherapistDetails WHERE CutitronicsTherapistID = %(CutitronicsTherapistID)s AND CutitronicsBrandID = %(CutitronicsBrandID)s"
    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsTherapistID': lv_CutitronicsTherapistID, 'CutitronicsBrandID': lv_CutitronicsBrandID})
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))

            if len(lv_list) == 0:
                return(300)
            else:
                return lv_list
    except pymysql.err.IntegrityError as e:
        logger.info(e.args)
        return(300)


def fn_CheckUserExists(lv_CutitronicsClientID, lv_CutitronicsBrandID):

    """
    Name - fn_CheckUserExists

    Description - Checks the backend database to see if the userid is valid

    Arguments -
        lv_CutitronicsClientID (String)

    Returns -
        300 - Failure, the user does not exist in the database
        Success - Cutitronics Client ID is returned, user exists in the back office
    """

    lv_list = ""
    lv_statement = "SELECT CutitronicsClientID, CutitronicsBrandID FROM ClientDetails WHERE CutitronicsClientID = %(CutitronicsClientID)s AND CutitronicsBrandID = %(CutitronicsBrandID)s AND DeletionFlag IS NULL ORDER BY 1;"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsClientID': lv_CutitronicsClientID, 'CutitronicsBrandID': lv_CutitronicsBrandID})
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))

            if len(lv_list) == 0:
                return(300)
            else:
                return(lv_list)
    except pymysql.err.IntegrityError as e:
        logger.info(e.args)
        return(300)


def fn_ReturnLookUpCode(lv_CutitronicsBrandID, lv_LookUpType, lv_SessionTypeCode):

    """
    Name - fn_ReturnLookUpCode

    Description - Checks the backend database to see if the LookUpCode (SessionType) is valid

    Arguments -
        lv_LookUpType (String)
        lv_SessionTypeCode (String)

    Returns -
        300 - Failure, the Treatment type does not exist in the database
        Success - CutitronicsLookUpCode is returned, treatment type exists in the back office
    """

    lv_list = ""
    lv_statement = "SELECT CutitronicsBrandID, CutitronicsLookUpType, CutitronicsLookUpCode, CutitronicsDesc FROM CutitronicsLookUp WHERE CutitronicsBrandID = %(CutitronicsBrandID)s AND CutitronicsLookUpType = %(CutitronicsLookUpType)s AND CutitronicsLookUpCode = %(SessionTypeCode)s"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsBrandID': lv_CutitronicsBrandID, 'CutitronicsLookUpType': lv_LookUpType, 'SessionTypeCode': lv_SessionTypeCode})
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))

            if len(lv_list) == 0:
                return 300
            else:
                return lv_list
    except pymysql.err.IntegrityError as e:
        logger.info(e.args)
        return 300


def fn_createPrescriptionID():
    """
    Name - fn_createTherapistSessionID

    Description - Returns a unique CutitronicsSessionID in correct format

    Arguments - N/A

    Returns:
        - String: CutitronicsSessionID Value
    """

    return str(uuid.uuid4())


def fn_CreateTherapistSession(lv_CutitronicsSessionID, lv_CutitronicsTherapistID, lv_CutitronicsClientID, lv_SessionTypeCode, lv_Source):
    """
    Name - fn_createTherapistSession

    Description - Creates a therapist session object within the database

    Arguments:
        lv_CutitronicsSessionID (String)
        lv_CutitronicsTherapistID (String)
        lv_CutitronicsClientID (String)
        lv_SessionTypeCode (String)
        lv_Source (String)

    Returns:
        300 - Failure, unable to create session object in TherapistSession
        100 - Success, A new Therapist Session object has been created in the back office
    """
    lv_statement = "INSERT INTO TherapistSession (CutitronicsSessionID, CutitronicsTherapistID, CutitronicsClientID, SessionTypeCode, SessionStatus, SessionStart, SessionEnd, CurrentTemperature, CreationDate, CreationUser, LastUpdateDate, LastUpdateUser, Attribute1, Attribute2, Attribute3) VALUES (%(CutitronicsSessionID)s, %(CutitronicsTherapistID)s, %(CutitronicsClientID)s, %(SessionTypeCode)s, 'Open', SYSDATE(), NULL, NULL, SYSDATE(), %(CreationUser)s, SYSDATE(), NULL, NULL, NULL, NULL)"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsSessionID': lv_CutitronicsSessionID, 'CutitronicsTherapistID': lv_CutitronicsTherapistID, 'CutitronicsClientID': lv_CutitronicsClientID, 'SessionTypeCode': lv_SessionTypeCode, 'CreationUser': lv_Source})
            if cursor.rowcount == 1:
                return(100)
    except pymysql.err.IntegrityError as e:
        logger.info(e.args)
        return 300


def fn_CreateBrandPrescription(lv_CutitronicsPrescriptionID, lv_CutitronicsBrandID, lv_PrescriptionName, lv_SkinType, lv_CreationUser):

    lv_list = ""
    lv_statement = "INSERT INTO BrandPrescriptions (CutitronicsPresID, CutitronicsBrandID, PrescriptionName, SkinType, CreationDate, CreationUser, LastUpdateDate, LastUpdateUser, Attribute1, Attribute2, Attribute3) VALUES (%(CutitronicsPresID)s, %(CutitronicsBrandID)s, %(PrescriptionName)s, %(SkinType)s, SYSDATE(), %(CreationUser)s, SYSDATE(), %(CreationUser)s, NULL, NULL, NULL)"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsPresID': lv_CutitronicsPrescriptionID, 'CutitronicsBrandID': lv_CutitronicsBrandID, 'PrescriptionName':lv_PrescriptionName, 'SkinType': lv_SkinType, 'CreationUser': lv_CreationUser})
            if cursor.rowcount == 1:
                return(100)
    except pymysql.err.IntegrityError as e:
        logger.info(e.args)
        return 500



def lambda_handler(event, context):
    """
    createSession

    Creates a new Therapist Session
    """
    openConnection()

    logger.info("Lambda function - {function_name}".format(function_name=context.function_name))

    # Variables

    lv_msgVersion = None
    lv_testFlag = None
    lv_CutitronicsPrescriptionID = None
    lv_CutitronicsBrandID = None
    lv_PrescriptionName = None
    lv_SkinType = None
    lv_SessionTypeCode = None
    lv_Source = None

    # Return block

    CreateBrandPrescription_out = {"Service": context.function_name, "Status": "Success", "ErrorCode": "", "ErrorDesc": "", "ErrorStack": "", "ServiceOutput": {"CutitronicsPresID": lv_CutitronicsPrescriptionID, "CutitronicsBrandID": lv_CutitronicsBrandID, "PrescriptionName": lv_PrescriptionName, "SkinType": lv_SkinType}}

    try:
        try:
            body = json.loads(event['body'])
        except KeyError as e:
            try:
                body = event
            except TypeError:
                logger.error("The payload is not in the right format")
                app_json = json.dumps(event)
                event = {"body": app_json}
                body = json.loads(event['body'])

        logger.info("Payload body - '{body}'".format(body=body))
        logger.info(" ")
        logger.info("Payload event - '{lv_event}'".format(lv_event=event))

        lv_msgVersion = body.get('msgVersion', 0)
        lv_testFlag = body.get('testFlag', 0)
        lv_CutitronicsBrandID = body.get('CutitronicsBrandID', 0)
        lv_PrescriptionName = body.get('PrescriptionName', "")
        lv_SkinType = body.get('SkinType', "0")

        # old vars
        lv_Source = body.get("Source", "")

        '''
        1. Does Brand exist
        '''

        logger.info(" ")
        logger.info("Calling fn_checkBrandExist to check if the following Brand exists - %s", lv_CutitronicsBrandID)
        logger.info(" ")

        lv_BrandType = fn_checkBrandExist(lv_CutitronicsBrandID)

        logger.info(" ")
        logger.info("fn_checkBrandExist return - %s", lv_BrandType)
        logger.info(" ")

        if lv_BrandType == 500:  # Failure

            logger.error("Brand '{lv_CutitronicsBrandID}' has not been found in the back office systems".format(lv_CutitronicsBrandID=lv_CutitronicsBrandID))

            CreateBrandPrescription_out['Status'] = "Error"
            CreateBrandPrescription_out['ErrorCode'] = context.function_name + "_001"  # CreateTherapistSession_001
            CreateBrandPrescription_out['ErrorDesc'] = "Supplied brandID does not exist in the BO Database"
            CreateBrandPrescription_out["ServiceOutput"]["CutitronicsBrandID"] = body.get('CutitronicsBrandID', "")
            CreateBrandPrescription_out["ServiceOutput"]["PrescriptionName"] = lv_PrescriptionName
            CreateBrandPrescription_out["ServiceOutput"]["SkinType"] = lv_SkinType

            # Lambda response
            Connection.close()
            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(CreateBrandPrescription_out)
            }

        '''
        2. Create Prescription ID
        '''
        
        lv_CutitronicsPrescriptionID = fn_createPrescriptionID()

        logger.info(" ")
        logger.info("Generating Session code - %s", lv_CutitronicsPrescriptionID)
        logger.info(" ")

        '''
        8. Create Brand Prescription
        '''
        lv_CreateBrandPrescription = fn_CreateBrandPrescription(lv_CutitronicsPrescriptionID, lv_CutitronicsBrandID, lv_PrescriptionName, lv_SkinType, 'createPrescription')

        if lv_CreateBrandPrescription == 300:  # Failure

            logger.error("Prescription '{lv_Session}' has not been created in the back office systems".format(lv_Session=lv_CutitronicsPrescriptionID))

            CreateBrandPrescription_out['Status'] = "Error"
            CreateBrandPrescription_out['ErrorCode'] = context.function_name + "_005"  # CreateTherapistSession_005
            CreateBrandPrescription_out['ErrorDesc'] = "Couldn't create session in database"
            CreateBrandPrescription_out["ServiceOutput"]["CutitronicsBrandID"] = body.get('CutitronicsBrandID', "")
            CreateBrandPrescription_out["ServiceOutput"]["PrescriptionName"] = lv_PrescriptionName
            CreateBrandPrescription_out["ServiceOutput"]["SkinType"] = lv_SkinType

            Connection.close()
            # Lambda response

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(CreateBrandPrescription_out)
            }

        # Create output

        Connection.commit()
        Connection.close()

        logger.info("Created Prescription with CutitronicsPresID: %s", lv_CutitronicsPrescriptionID)

        CreateBrandPrescription_out['Status'] = "Success"
        CreateBrandPrescription_out['ErrorCode'] = ""
        CreateBrandPrescription_out['ErrorDesc'] = ""
        CreateBrandPrescription_out["ServiceOutput"]["CutitronicsPresID"] = lv_CutitronicsPrescriptionID
        CreateBrandPrescription_out["ServiceOutput"]["CutitronicsBrandID"] = body.get('CutitronicsBrandID', "")
        CreateBrandPrescription_out["ServiceOutput"]["PrescriptionName"] = lv_PrescriptionName
        CreateBrandPrescription_out["ServiceOutput"]["SkinType"] = lv_SkinType

        return {
            "isBase64Encoded": "false",
            "statusCode": 200,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(CreateBrandPrescription_out)
        }

    except Exception as e:
        logger.error("CATCH-ALL ERROR: Unexpected error: '{error}'".format(error=e))

        exception_type, exception_value, exception_traceback = sys.exc_info()
        traceback_string = traceback.format_exception(exception_type, exception_value, exception_traceback)
        err_msg = json.dumps({
            "errorType": exception_type.__name__,
            "errorMessage": str(exception_value),
            "stackTrace": traceback_string
        })
        logger.error(err_msg)

        Connection.rollback()
        Connection.close()

        # Populate Cutitronics reply block

        CreateBrandPrescription_out['Status'] = "Error"
        CreateBrandPrescription_out['ErrorCode'] = context.function_name + "_000"  # CreateTherapistSession_000
        CreateBrandPrescription_out['ErrorDesc'] = "CatchALL"
        CreateBrandPrescription_out["ServiceOutput"]["CutitronicsBrandID"] = body.get('CutitronicsBrandID', "")
        CreateBrandPrescription_out["ServiceOutput"]["PrescriptionName"] = lv_PrescriptionName
        CreateBrandPrescription_out["ServiceOutput"]["SkinType"] = lv_SkinType

        # Lambda response

        return {
            "isBase64Encoded": "false",
            "statusCode": 500,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(CreateBrandPrescription_out)
        }
